﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DapperDemo.Database;
using DapperDemo.Models;
using DapperDemo.Repository;

namespace DapperDemo.Controllers
{
    
    public class CompaniesController : Controller
    {
        private readonly ICompanyRepository _compRepo;
        private readonly IEmployeeRepository _empRepo;
        private readonly IBonusRepository _bonRepo;
        private readonly IDapperSprocRepo _dapperRepo;

        [BindProperty]
        public Company Company { get; set; }

        public CompaniesController(ICompanyRepository compRepo, IEmployeeRepository empRepo, IBonusRepository bonRepo, IDapperSprocRepo dapperRepo)
        {
            _compRepo = compRepo;
            _empRepo = empRepo;
            _bonRepo = bonRepo;
            _dapperRepo = dapperRepo;
        }
    
        // GET: Companies
        public IActionResult Index()
        {
            //return View(_compRepo.GetAll());
            return View(_dapperRepo.List<Company>("usp_GetALLCompany"));
        }

        // GET: Companies/Details/5
        public  IActionResult Details(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }

            var company = _bonRepo.GetCompanyWithEmployees(Id.GetValueOrDefault());
            if (company == null)
            {
                return NotFound();
            }

            return View(company);
        }

        // GET: Companies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName ("Create")]
        public IActionResult CreatePost()
        {
            if (ModelState.IsValid)
            {
                _compRepo.Add(Company);
                return RedirectToAction(nameof(Index));
            }
            return View(Company);
        }

        // GET: Companies/Edit/5
        public IActionResult Edit(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }
            var company = _dapperRepo.Single<Company>("usp_GetCompany", new { CompanyId = Id.GetValueOrDefault() });
            //var company = _compRepo.Find(Id.GetValueOrDefault());
            if (company == null)
            {
                return NotFound();
            }
            return View(company);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int Id, [Bind("CompanyId,Name,Address,City,State,Pincode")] Company company)
        {
            if (Id != company.CompanyId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _compRepo.Update(company);
                return RedirectToAction(nameof(Index));
            }
            return View(company);
        }

        // GET: Companies/Delete/5
        public IActionResult Delete(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }
            _compRepo.Remove(Id.GetValueOrDefault());
            return RedirectToAction(nameof(Index));
        }
    }
}
