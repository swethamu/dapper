﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DapperDemo.Database;
using DapperDemo.Models;
using DapperDemo.Repository;

namespace DapperDemo.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly IEmployeeRepository _empRepo;  
        private readonly ICompanyRepository _compRepo;
        private readonly IBonusRepository _bonRepo;

        [BindProperty]
        public Employee Employee { get; set; }

        public EmployeesController(ICompanyRepository compRepo, IEmployeeRepository empRepo, IBonusRepository bonRepo)
        {
            _compRepo = compRepo;
            _bonRepo = bonRepo;
            _empRepo = empRepo;
        }
    
        // GET: Employees
        public IActionResult Index(int companyId=0)
        {
            //List<Employee> employees = _empRepo.GetAll();
            //foreach (Employee obj in employees)
            //{
            //    obj.Company = _compRepo.Find(obj.CompanyId);
            //
            List<Employee> employees = _bonRepo.GetEmployeeWithCompany(companyId);
            return View(employees);
        }

        // GET: Employees/Details/5
        public  IActionResult Details(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }

            var employee = _empRepo.Find(Id.GetValueOrDefault());
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        //GET: Employees/Create
        public IActionResult Create()
        {
            IEnumerable<SelectListItem> CompanyList = _compRepo.GetAll().Select(i => new SelectListItem
            {
                Text = i.Name,
                Value = i.CompanyId.ToString()
            });
            ViewBag.CompanyList = CompanyList;

            return View();
        }

        // POST: Employees/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName ("Create")]
        public async Task<IActionResult> CreatePost()
        {
            if (ModelState.IsValid)
            {
               await _empRepo.AddAsync(Employee);
                return RedirectToAction(nameof(Index));
            }
            return View(Employee);
        }

        // GET: Employees/Edit/5
        public IActionResult Edit(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }

              Employee = _empRepo.Find(Id.GetValueOrDefault());
            IEnumerable<SelectListItem> CompanyList = _compRepo.GetAll().Select(i => new SelectListItem
            {
                Text = i.Name,
                Value = i.CompanyId.ToString()
            });
            ViewBag.CompanyList = CompanyList;
            if (Employee == null)
            {
                return NotFound();
            }
            return View(Employee);
        }

        // POST: Employees/Edit/5
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int Id)
        {
            if (Id != Employee.EmployeeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _empRepo.Update(Employee);
                return RedirectToAction(nameof(Index));
            }

            return View(Employee);
        }

        // GET: Employees/Delete/5
        public IActionResult Delete(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }
            _empRepo.Remove(Id.GetValueOrDefault());
            return RedirectToAction(nameof(Index));
        }
    }
}
