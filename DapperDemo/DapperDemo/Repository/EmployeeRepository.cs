﻿using Dapper;
using DapperDemo.Database;
using DapperDemo.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DapperDemo.Repository
{
    public  class EmployeeRepository : IEmployeeRepository
    {

        private IDbConnection db;
        public EmployeeRepository(IConfiguration configuration)
        {
            this.db = new SqlConnection(configuration.GetConnectionString("DefaultConnections"));
        }

        public Employee Add(Employee employee)
        {
            var sql ="INSERT INTO Employees (Name,Title,Email,Phone,CompanyId)VALUES(@Name,@Title,@Email,@Phone,@CompanyId)"
                + "SELECT CAST(SCOPE_IDENTITY() as int);";
            var Id = db.Query<int>(sql,employee).Single();
            employee.EmployeeId = Id;
            return employee;
        }
        public async Task<Employee> AddAsync(Employee employee)
        {
            var sql = "INSERT INTO Employees (Name,Title,Email,Phone,CompanyId)VALUES(@Name,@Title,@Email,@Phone,@CompanyId)"
                + "SELECT CAST(SCOPE_IDENTITY() as int);";
            var Id = await db.QueryAsync<int>(sql, employee);
            employee.EmployeeId = Id.Single();
            return employee;
        }

        public Employee Find(int Id)
        {
            var sql = "SELECT * FROM Employees WHERE EmployeeId= @EmployeeId";
            return db.Query<Employee>(sql, new { @EmployeeId = Id }).Single();
            
        }

        public List<Employee> GetAll()
        {
            var sql = "SELECT * FROM Employees";
            return db.Query<Employee>(sql).ToList();
        }

        public void Remove(int Id)
        {
            var sql = "DELETE FROM Employees WHERE EmployeeId= @Id";
            db.Execute(sql, new { Id });
        }


        public Employee Update(Employee employee)
        {
            var sql = "UPDATE Employees SET Name=@Name,Email=@Email,Phone=@Phone,Title=@Title,CompanyId=@CompanyId WHERE EmployeeId=@EmployeeId";
            db.Execute(sql, employee);
            return employee;

        }
    }
}
