﻿using DapperDemo.Database;
using DapperDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DapperDemo.Repository
{
    public class CompanyRepositoryEf : ICompanyRepository
    {
        private readonly ApplicationDbContext _db;

        public CompanyRepositoryEf(ApplicationDbContext db)
        {
            _db= db;
        }

        public Company Add(Company company)
        {
            _db.Companies.Add(company);
            _db.SaveChanges();
            return company;
        }

        public Company Find(int Id)
        {
            return _db.Companies.Find(Id);
        }

        public List<Company> GetAll()
        {
            return _db.Companies.ToList();

        }

        public void Remove(int Id)
        {
            Company company = _db.Companies.FirstOrDefault(u => u.CompanyId == Id);
            _db.Companies.Remove(company);
            _db.SaveChanges();
            return;
        }


        public Company Update(Company company)
        {
            _db.Companies.Update(company);
            _db.SaveChanges();
            return company;
        }
    }
}
