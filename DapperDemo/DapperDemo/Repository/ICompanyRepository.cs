﻿using DapperDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DapperDemo.Repository
{
   public  interface ICompanyRepository
    {
        Company Find(int Id);
        List<Company> GetAll();
        Company Add(Company company);
        Company Update(Company company);
        void Remove(int Id);
    }
}
