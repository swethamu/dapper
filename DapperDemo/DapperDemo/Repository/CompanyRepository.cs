﻿using Dapper;
using DapperDemo.Database;
using DapperDemo.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DapperDemo.Repository
{
    public class CompanyRepository : ICompanyRepository
    {

        private IDbConnection db;
        public CompanyRepository(IConfiguration configuration)
        {
            this.db = new SqlConnection(configuration.GetConnectionString("DefaultConnections"));
        }

        public Company Add(Company company)
        {
            var sql = "INSERT INTO Companies(Name,Address,City,State,Pincode)VALUES(@Name,@Address,@City,@State,@Pincode)"
                + "SELECT CAST(SCOPE_IDENTITY() as int);";
            var Id = db.Query<int>(sql,company).Single();
            company.CompanyId = Id;
            return company;
        }
        public Company Find(int Id)
        {
            var sql = "SELECT * FROM Companies WHERE CompanyId= @CompanyId";
            return db.Query<Company>(sql, new { @CompanyId = Id }).Single();
            
        }

        public List<Company> GetAll()
        {
            var sql = "SELECT * FROM Companies";
            return db.Query<Company>(sql).ToList();
        }

        public void Remove(int Id)
        {
            var sql = "DELETE FROM Companies WHERE CompanyId= @Id";
            db.Execute(sql, new { Id });
        }


        public Company Update(Company company)
        {
            var sql = "UPDATE Companies SET Name=@Name,Address=@Address,City=@City,State=@State,Pincode=@Pincode WHERE CompanyId=@CompanyId";
            db.Execute(sql, company);
            return company;

        }
    }
}
