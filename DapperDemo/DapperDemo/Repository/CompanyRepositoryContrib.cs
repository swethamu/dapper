﻿using Dapper;
using Dapper.Contrib.Extensions;
using DapperDemo.Database;
using DapperDemo.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DapperDemo.Repository
{
    public class CompanyRepositoryContrib : ICompanyRepository
    {

        private IDbConnection db;
        public CompanyRepositoryContrib(IConfiguration configuration)
        {
            this.db = new SqlConnection(configuration.GetConnectionString("DefaultConnections"));
        }

        public Company Add(Company company)
        {
            var Id = db.Insert(company);
            company.CompanyId = (int)Id;
            return company;
        }
            public Company Find(int Id)
        {

            return db.Get<Company>(Id);

        }  
        public List<Company> GetAll()
        {
            
            return db.GetAll<Company>().ToList();
        }

        public void Remove(int Id)
        {
            db.Delete( new Company { CompanyId = Id });
        }


        public Company Update(Company company)
        {
            db.Update(company);
            return (company);
          
        }
    } 
}
