﻿using Dapper;
using DapperDemo.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace DapperDemo.Repository
{
    public class BonusRepository : IBonusRepository
    {

        private IDbConnection db;
        public BonusRepository(IConfiguration configuration)
        {
            this.db = new SqlConnection(configuration.GetConnectionString("DefaultConnections"));
        }

        public void AddTestCompanyWithEmployees(Company objComp)
        {
            var sql = "INSERT INTO Companies(Name,Address,City,State,Pincode)VALUES(@Name,@Address,@City,@State,@Pincode)"
                 + "SELECT CAST(SCOPE_IDENTITY() as int);";
            var Id = db.Query<int>(sql, objComp).Single();
            objComp.CompanyId = Id;

            //foreach (var employee in objComp.Employees)
            //{
            //    employee.CompanyId = objComp.CompanyId;
            //   var sql1 = "INSERT INTO Employees (Name,Title,Email,Phone,CompanyId)VALUES(@Name,@Title,@Email,@Phone,@CompanyId)"
            //        + "SELECT CAST(SCOPE_IDENTITY() as int);";
            //    db.Query<int>(sql1, employee).Single();

            //}
            objComp.Employees.Select(c => { c.CompanyId = Id; return c; }).ToList();
            var sqlEmp = "INSERT INTO Employees (Name,Title,Email,Phone,CompanyId)VALUES(@Name,@Title,@Email,@Phone,@CompanyId)"
                + "SELECT CAST(SCOPE_IDENTITY() as int);";
            db.Execute(sqlEmp, objComp.Employees);
        }



        public List<Company> GetAllCompanyWithEmployees()
        {
            var sql = "SELECT C.*,E.* FROM Employees AS E INNER JOIN Companies AS C ON E.CompanyId = C.CompanyId ";
            var companyDic = new Dictionary<int, Company>();
            var company = db.Query<Company, Employee, Company>(sql, (c, e) =>
              {
                  if (!companyDic.TryGetValue(c.CompanyId, out var currentCompany))
                  {
                      currentCompany = c;
                      companyDic.Add(currentCompany.CompanyId, currentCompany);
                  }
                  currentCompany.Employees.Add(e);
                  return currentCompany;
              }, splitOn: "EmployeeId");
            return company.Distinct().ToList();
        }

        public Company GetCompanyWithEmployees(int Id)
        {
            var p = new
            {
                CompanyId = Id
            };
            var sql = "SELECT * FROM Companies WHERE CompanyId =@CompanyId;"
                + "Select * From Employees WHERE CompanyId =@CompanyId";
            Company company;
            using (var lists=db.QueryMultiple(sql,p))
            {
                company = lists.Read<Company>().ToList().FirstOrDefault();
                company.Employees = lists.Read<Employee>().ToList();
            }
            return (company);
        }

        public List<Employee> GetEmployeeWithCompany(int Id)
        {

            var sql = "SELECT E.*,C.* FROM Employees AS E INNER JOIN Companies AS C ON E.CompanyId = C.CompanyId ";

            if (Id != 0)
            {
                sql += "WHERE E.CompanyId = @Id";
            }

            var employee = db.Query<Employee, Company, Employee>(sql, (e, c) =>
            {
                e.Company = c;
                return e;
            }, new { Id }, splitOn: "CompanyId");
            return employee.ToList();

        }

        public void RemoveRange(int[] companyId)
        {
            db.Query("DELETE FROM  Companies WHERE CompanyId IN @companyId", new { companyId });
        }
        public List<Company> FilterCompanyByName(string name)
        {
            return db.Query<Company>("SELECT * FROM Companies WHERE Name like '%' + @name + '%' ", new { name }).ToList();
        }
        public void AddTestCompanyWithEmployeesWithTransaction(Company objComp)
        {
            using (var transaction = new TransactionScope())
            {
                try
                {
                    var sql = "INSERT INTO Companies(Name,Address,City,State,Pincode)VALUES(@Name,@Address,@City,@State,@Pincode)"
                    + "SELECT CAST(SCOPE_IDENTITY() as int);";
                    var Id = db.Query<int>(sql, objComp).Single();
                    objComp.CompanyId = Id;

                    objComp.Employees.Select(c => { c.CompanyId = Id; return c; }).ToList();
                    var sqlEmp = "INSERT INTO Employees (Name,Title,Email,Phone,CompanyId)VALUES(@Name,@Title,@Email,@Phone,@CompanyId)"
                        + "SELECT CAST(SCOPE_IDENTITY() as int);";
                    db.Execute(sqlEmp, objComp.Employees);
                    transaction.Complete();
                }
                catch (Exception ex)
                {

                }
            }
           

            //foreach (var employee in objComp.Employees)
            //{
            //    employee.CompanyId = objComp.CompanyId;
            //   var sql1 = "INSERT INTO Employees (Name,Title,Email,Phone,CompanyId)VALUES(@Name,@Title,@Email,@Phone,@CompanyId)"
            //        + "SELECT CAST(SCOPE_IDENTITY() as int);";
            //    db.Query<int>(sql1, employee).Single();

            //}
           
        }
    }
}
